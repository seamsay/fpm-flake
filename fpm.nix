{
  stdenv,
  lib,
  fetchFromGitHub,
  fetchurl,
  makeBinaryWrapper,
  cacert,
  gfortran,
  git,
}: let
  pname = "fpm";
  version = "0.7.0";

  bootstrapper = fetchurl {
    url = "https://github.com/fortran-lang/fpm/releases/download/v${version}/fpm-${version}.F90";
    hash = "sha256-0W5FtwUCnW04sLp8zKsrnttIwvKUQUc1SLhm2gOJJD4=";
  };

  bootdir = "build/bootstrap";

  FC = "${gfortran}/bin/gfortran";
  FFLAGS = "-g -fbacktrace -O3";
in
  stdenv.mkDerivation {
    inherit pname version;

    src = fetchFromGitHub {
      owner = "fortran-lang";
      repo = pname;
      rev = "v${version}";
      hash = "sha256-oP/neQvdQrXorNGq2z+fWwReOJ2tgoLN9TUCXiGcwMo=";
    };

    depsBuildBuild = [gfortran];
    nativeBuildInputs = [cacert makeBinaryWrapper];
    buildInputs = [git];

    inherit FC FFLAGS;

    buildPhase = ''
      runHook preBuild

      mkdir -p ${bootdir}
      $FC -J ${bootdir} $FFLAGS -o ${bootdir}/fpm ${bootstrapper}
      ${bootdir}/fpm update

      runHook postBuild
    '';

    installPhase = ''
      runHook preInstall

      mkdir $out
      ${bootdir}/fpm install --compiler "$FC" --flag "$FFLAGS" --prefix "$out"

      runHook postInstall
    '';

    # TODO: These things are only needed when this flake is used as a dependency to another flake. Figure out why because it makes me think I'm doing something wrong.
    postFixup = ''
      wrapProgram $out/bin/fpm \
        --set NIX_SSL_CERT_FILE "${cacert}/etc/ssl/certs/ca-bundle.crt" \
        --suffix PATH : "${lib.makeBinPath [gfortran git]}"
    '';

    meta = {
      homepage = "https://fpm.fortran-lang.org/";
      description = "Fortran package manager.";
      license = lib.licenses.mit;
    };
  }
