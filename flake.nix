{
  inputs = {
    flake-utils.url = "github:numtide/flake-utils";
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
  };

  outputs = {
    self,
    flake-utils,
    nixpkgs,
  }:
    flake-utils.lib.eachDefaultSystem (system: let
      pkgs = nixpkgs.legacyPackages.${system};
      selfFormatter = self.formatter.${system};
      selfFornixate = self.fornixate.${system};
      selfPackages = self.packages.${system};
    in {
      packages.default = selfPackages.fpm;
      packages.fpm = pkgs.callPackage ./fpm.nix {};

      fornixate = pkgs.callPackage ./fornixate.nix {
        fpm = selfPackages.fpm;
        pylama = pkgs.python3.pkgs.pylama;
      };

      # Can't use `callPackage` because it introduces things into `checks` that aren't derivations.
      checks = import ./checks.nix {
        inherit (pkgs) alejandra lib stdenv writeShellApplication;
        currentFormatter = selfFormatter;
        fornixate = selfFornixate;
        fpm = selfPackages.fpm;
      };

      formatter = pkgs.alejandra;
    });
}
