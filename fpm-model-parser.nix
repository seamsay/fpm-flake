{
  black,
  lib,
  pylama,
  python3,
  runCommand,
  writeTextFile,
}: let
  parser = writeTextFile {
    name = "fpm-model-parser.py";
    text = ''
      import pyparsing as pp
      from pyparsing import pyparsing_common as ppc


      def argument(key, value):
          return pp.Group(pp.CaselessLiteral(key) + pp.Suppress("=") + value)


      def named_t(name, arguments):
          return (
              pp.Suppress(pp.CaselessLiteral(name))
              + pp.Suppress("(")
              + pp.Group(pp.Dict(pp.delimited_list(arguments, allow_trailing_delim=True)))
              + pp.Suppress(")")
          )


      def anonymous_t(arguments):
          return (
              pp.Suppress("(")
              + pp.Group(pp.Dict(pp.delimited_list(arguments, allow_trailing_delim=True)))
              + pp.Suppress(")")
          )


      def array(of):
          return (
              pp.Suppress("[")
              + pp.Group(pp.Optional(pp.delimited_list(of, allow_trailing_delim=True)))
              + pp.Suppress("]")
          )


      string = pp.QuotedString('"') | pp.QuotedString("'")

      scope = (
          pp.CaselessLiteral("FPM_SCOPE_APP")
          | pp.CaselessLiteral("FPM_SCOPE_EXAMPLE")
          | pp.CaselessLiteral("FPM_SCOPE_LIB")
          | pp.CaselessLiteral("FPM_SCOPE_TEST")
      )

      unit = (
          pp.CaselessLiteral("FPM_UNIT_CSOURCE")
          | pp.CaselessLiteral("FPM_UNIT_MODULE")
          | pp.CaselessLiteral("FPM_UNIT_PROGRAM")
          | pp.CaselessLiteral("FPM_UNIT_SUBMODULE")
          | pp.CaselessLiteral("FPM_UNIT_SUBPROGRAM")
      )

      srcfile_t = named_t(
          "srcfile_t",
          argument("file_name", string)
          | argument("exe_name", string)
          | argument("unit_scope", scope)
          | argument("modules_provided", array(string))
          | argument("parent_modules", array(string))
          | argument("unit_type", unit)
          | argument("modules_used", array(string))
          | argument("include_dependencies", array(string))
          | argument("link_libraries", array(string))
          | argument("digest", ppc.signed_integer),
      )

      package_t = named_t(
          "package_t", argument("name", string) | argument("sources", array(srcfile_t))
      )

      compiler_t = anonymous_t(argument("fc", string) | argument("cc", string))

      archiver_t = anonymous_t(argument("ar", string))

      dependency_tree_t = (
          pp.Suppress(pp.CaselessLiteral("dependency_tree_t"))
          + pp.Suppress("(")
          + "..."
          + pp.Suppress(")")
      )

      fpm_model_t = named_t(
          "fpm_model_t",
          argument("package_name", string)
          | argument("packages", array(package_t))
          | argument("compiler", compiler_t)
          | argument("archiver", archiver_t)
          | argument("fortran_compile_flags", string)
          | argument("c_compile_flags", string)
          | argument("cxx_compile_flags", string)
          | argument("link_flags", string)
          | argument("build_prefix", string)
          | argument("link_libraries", array(string))
          | argument("external_modules", array(string))
          | argument("deps", dependency_tree_t),
      )

      if __name__ == "__main__":
          import json
          import sys

          input = sys.stdin.read()

          model = fpm_model_t.parse_string(input)[0].as_dict()
          # We only care about the packages, and other elements (e.g. the compiler flags) can
          # contain nix store paths, causing issues when we try to import it.
          data = model["packages"]

          json.dump(data, sys.stdout)
    '';
    checkPhase = ''
      ${black}/bin/black --check "$target"
      ${pylama}/bin/pylama "$target"
    '';
  };
in
  file: let
    json = runCommand "fpm_model_t.json" {buildInputs = [(python3.withPackages (pkgs: [pkgs.pyparsing]))];} ''
      python3 ${parser} <${file} >"$out"
    '';
  in
    lib.trivial.importJSON json
