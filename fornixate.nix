{
  callPackage,
  fpm,
  lib,
  pylama,
  remarshal,
  runCommand,
  stdenv,
}: let
  # Dependencies need to be deduplicated _from the right_, keeping each duplicated dependency in it's _rightmost_ position.
  # Going back to our A B C example (see below), we could conceivably end up with A B C B prior to deduplication which would lead to A B C upon applying `unique`.
  # By reversing the list before and after deduplication we ensure that we correctly get A C B instead.
  # The same logic must also be applied to library links.
  uniqueFromRight = list:
    lib.trivial.pipe list [
      lib.lists.reverseList
      lib.lists.unique
      lib.lists.reverseList
    ];

  parseFPMModel = callPackage ./fpm-model-parser.nix {inherit pylama;};

  buildProject = {
    src,
    resolvedDependencies,
    overrideInstall ? true,
  }: let
    mkIncludeFlag = drv: "-I${drv}/include";
    mkLibFlag = drv: "-L${drv}/lib";

    # SAFETY: `src` is referenced in `oldSrc`.
    srcPath = builtins.unsafeDiscardStringContext "${src}";

    oldInfo = lib.trivial.importTOML /${srcPath}/fpm.toml;
    oldSrc = builtins.filterSource (path: _: baseNameOf path != "fpm.toml") src;

    mkDependencyInfo = {
      name,
      src,
      resolvedDependencies,
    }:
      {inherit name;} // buildProject {inherit src resolvedDependencies;};

    dependencyProjects =
      if oldInfo ? dependencies
      then
        lib.trivial.pipe oldInfo.dependencies [
          (builtins.mapAttrs (name: _: builtins.getAttr name resolvedDependencies))
          (builtins.mapAttrs (_: info:
            builtins.fetchGit {
              url = info.git;
              rev = info.rev;
              allRefs = true;
            }))
          (lib.attrsets.mapAttrsToList (name: src: mkDependencyInfo {inherit name src resolvedDependencies;}))
        ]
      else [];

    # IMPORTANT: Dependencies are _ordered_ and therefore we must treat them as a list and not an attrset.
    #            When the dependencies are passed to the linker, dependencies which depend on other dependencies must come **before** _their_ dependencies.
    #            So if project A depends on projects B and C, but project C depends on project B, then the projects must be passed to the linker in the order A C B.
    #            It doesn't matter what the order is in other situations so we may as well always pass them around in linker order.

    directDependencies = map (dependency: {inherit (dependency) name drv library modules;}) dependencyProjects;
    transitiveDependencies = lib.trivial.pipe dependencyProjects [
      (map (builtins.getAttr "dependencies"))
      lib.lists.flatten
    ];
    dependencies = uniqueFromRight (directDependencies ++ transitiveDependencies);

    dependencyIncludeFlags = lib.trivial.pipe directDependencies [
      (map (builtins.getAttr "drv"))
      (map mkIncludeFlag)
    ];
    dependencyLibFlags = lib.trivial.pipe dependencies [
      (map (builtins.getAttr "drv"))
      (map mkLibFlag)
    ];
    dependencyFlags = lib.lists.unique (dependencyIncludeFlags ++ dependencyLibFlags);
    dependencyFlagsStr = builtins.concatStringsSep " " dependencyFlags;

    newInfo = let
      # TODO: It's not clear to me why `external-modules` need to be lowercase for FPM to find them, I think it might be an FPM bug.
      external-modules = let
        oldExternalModules = lib.attrsets.attrByPath ["build" "external-modules"] [] oldInfo;
        newExternalModules = lib.trivial.pipe directDependencies [
          (map (builtins.getAttr "modules"))
          lib.lists.flatten
        ];
        externalModules =
          (
            if builtins.isString oldExternalModules
            then [oldExternalModules]
            else oldExternalModules
          )
          ++ newExternalModules;
      in
        lib.trivial.pipe externalModules [
          uniqueFromRight
          (map lib.strings.toLower)
        ];

      link = let
        oldLink = lib.attrsets.attrByPath ["build" "link"] [] oldInfo;
        newLink =
          map (builtins.getAttr "library") dependencies;
      in
        uniqueFromRight (oldLink ++ newLink);

      newInfo' =
        oldInfo
        // {
          dependencies = {};
          build =
            (
              lib.attrsets.attrByPath ["build"] {} oldInfo
            )
            // {inherit external-modules link;};
        }
        // lib.attrsets.optionalAttrs overrideInstall {install.library = true;};
    in
      # TODO: This shouldn't _really_ be necessary...
      builtins.removeAttrs newInfo' ["dev-dependencies" "example" "test"];

    pname = newInfo.name;
    version =
      if newInfo ? version
      then newInfo.version
      else "0.1.0";

    # There is no `toTOML` available in nixpkgs, so we write it to JSON then use `remarshal`.
    newInfoJSON = builtins.toFile "fpm.json" (builtins.toJSON newInfo);
    newInfoTOML = runCommand "fpm.toml" {buildInputs = [remarshal];} ''
      remarshal --if json -i ${newInfoJSON} --of toml -o "$out"
    '';

    # TODO: Could we use `linkFarm` for this? Or is there something else?
    newSrc = runCommand "${pname}-${version}-source" {} ''
      mkdir "$out"
      cp -R '${oldSrc}'/* "$out/"
      cp '${newInfoTOML}' "$out/fpm.toml"
    '';

    projectModelPackage = let
      # Some people have checked their build directories into git, causing issues when FPM tries to create it.
      modelSrc = builtins.filterSource (path: _: baseNameOf path != "build") newSrc;

      text = runCommand "fpm_model_t.txt" {buildInputs = [fpm];} ''
        tempdir="$(mktemp -d)"
        cp -R '${modelSrc}'/* "$tempdir"
        cd "$tempdir"

        # On the first run FPM will print logging to stdout when creating the build directory.
        fpm build \
          --profile release \
          --flag '${dependencyFlagsStr}' \
          --show-model

        # For some cursed reason FPM will not print anything if you redirect to a file, so we must go via cat...
        fpm build \
          --profile release \
          --flag '${dependencyFlagsStr}' \
          --show-model | cat >"$out"
      '';

      packages = parseFPMModel text;
    in
      # It seems that the first package is always the package for the current project.
      builtins.elemAt packages 0;

    modules = lib.trivial.pipe projectModelPackage.sources [
      (builtins.filter (s: s.unit_scope == "FPM_SCOPE_LIB"))
      (map (builtins.getAttr "modules_provided"))
      lib.lists.flatten
    ];

    library = projectModelPackage.name;
  in {
    drv = stdenv.mkDerivation {
      inherit pname version;
      src = newSrc;

      depsBuildBuild = [fpm];
      nativeBuildInputs = map (builtins.getAttr "drv") dependencies;

      buildPhase = ''
        runHook preBuild

        fpm build \
          --profile release \
          --flag '${dependencyFlagsStr}'

        runHook postBuild
      '';

      installPhase = ''
        runHook preInstall

        mkdir "$out"
        fpm install \
          --profile release \
          --flag '${dependencyFlagsStr}' \
          --prefix "$out"

        runHook postInstall
      '';
    };
    inherit dependencies library modules;
  };
in
  {src}: let
    resolvedDependencies = lib.trivial.importTOML /${src}/fornixate.toml;
    src' = builtins.filterSource (path: _: baseNameOf path != "fornixate.toml") src;

    rootProject = buildProject {
      inherit resolvedDependencies;
      src = src';
      overrideInstall = false;
    };
  in
    rootProject.drv
