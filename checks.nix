{
  alejandra,
  currentFormatter,
  fornixate,
  fpm,
  lib,
  stdenv,
  writeShellApplication,
}: let
  mkTestFragment = name: ''
    echo -n 'Testing ${name}... '

    # TODO: I feel like a FIFO would be a better fit here, but it kept hanging when I tried to read from it...
    log="$(mktemp)"

    if (${name} >"$log" 2>&1)
    then
      echo 'passed!'
    else
      overall_status=3
      echo 'failed.'
      cat "$log"
    fi
  '';

  mkTestFragments = names: lib.strings.concatStringsSep "\n" (map mkTestFragment names);

  mkTestRunner = scripts:
    writeShellApplication {
      name = "fpm-flake-test-runner";
      runtimeInputs = scripts;
      text = ''
        overall_status=0

        ${mkTestFragments (map (builtins.getAttr "name") scripts)}

        exit "$overall_status"
      '';
    };

  globalSkip = [
    # TODO: Linking issues with C++ files.
    "datetime"
  ];

  testDir = ./tests;

  allTests = lib.trivial.pipe testDir [
    builtins.readDir
    (lib.attrsets.filterAttrs (_: val: val == "directory"))
    builtins.attrNames
    (map (name: {
      inherit name;
      path = /${testDir}/${name};
    }))
  ];

  filterTests = skip: builtins.filter (test: ! builtins.elem test.name skip) allTests;
in {
  # Test that all Nix files are formatted.
  formatted = assert currentFormatter == alejandra;
    writeShellApplication {
      name = "check-formatting";
      runtimeInputs = [alejandra];
      text = "alejandra --check .";
    };

  # Test that all tests in the corpus will build using the `fpm` executable.
  # This should also mean that `nix run` and `nix shell` work.
  run = let
    mkTestScript = test:
      writeShellApplication {
        name = test.name;
        runtimeInputs = [fpm];
        text = ''
          # Nix store is immutable, but `fpm` needs to create a build directory.
          tempdir="$(mktemp -d)"
          cp -R '${test.path}/'* "$tempdir"
          cd "$tempdir"
          fpm run
        '';
      };

    runSkip = globalSkip ++ [];
    runTests = filterTests runSkip;
  in
    mkTestRunner (map mkTestScript runTests);

  # Test that all tests in the corpus will build as flakes.
  # flake =

  # Test that all tests in the corpus will build with fornixate.
  fornixate = let
    mkTestScript = test: let
      drv = import test.path {inherit fornixate;};
    in
      writeShellApplication {
        name = test.name;
        runtimeInputs = [drv];
        text = "${drv.pname}";
      };

    fornixateSkip =
      globalSkip
      ++ [
        # TODO: Won't build.
        "finterp"
        "mctc-lib"
        "stdlib"
      ]
      ++ lib.lists.optionals (stdenv.isDarwin) [
        # TODO: Segmentation fault when running.
        "quaff"
      ];
    fornixateTests = filterTests fornixateSkip;
  in
    mkTestRunner (map mkTestScript fornixateTests);

  # Test that the test corpus is still up-to-date with the registry.
  # registry =
}
