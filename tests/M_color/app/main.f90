program main
  use M_color, only: rgbmono

  implicit none

  real :: grey
  integer :: ierr

  call rgbmono(100.0, 0.0, 0.0, grey, ierr)

  if (int(grey + 0.5) /= 30) then
    error stop "Failed to use M_color!"
  end if
end program main
