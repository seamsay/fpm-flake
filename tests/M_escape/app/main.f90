program main
  use M_escape, only: esc

  implicit none

  character(len=*), parameter :: expected = &
    char(27) // "[31mred" // char(27) // "[39m" // char(27) // "[0m"

  character(len=:), allocatable :: result

  result = esc("<r>red</r>")

  if (result /= expected) then
    error stop "Incorrect answer!"
  end if
end program main
