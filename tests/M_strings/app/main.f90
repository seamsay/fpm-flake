program main
  use M_strings, only: lower

  implicit none

  if (lower("HI") /= "hi") then
    error stop "Failed to use M_strings!"
  end if
end program main
