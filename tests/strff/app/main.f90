program main
  use iso_varying_string, only: char
  use strff, only: to_string

  implicit none

  if (char(to_string(42)) /= "42") then
    error stop "Failed to use strff!"
  end if
end program main
