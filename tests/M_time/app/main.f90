program main
  use M_time, only: d2u, u2d

  implicit none

  integer, dimension(8) :: datei, datef

  call date_and_time(values = datei)
  datef = u2d(d2u(datei))
  
  ! NOTE: The precision used for UNIX time in this library is too small for the
  !       round trip to work correctly, the milliseconds field will be wrong.
  if (any(datei(1:7) /= datef(1:7))) then
    error stop "Failed to use M_time!"
  end if
end program main
