program main
  use, intrinsic :: iso_fortran_env, only: int64, real64

  implicit none

  real(real64), parameter :: target = 3
  real(real64) :: result

  result = solve(target)
  if (abs(result - 0.18609104433037682) >= 1e-6_real64) then
    error stop "Incorrect result."
  end if
contains
  pure function solve(target) result(theta)
    real(real64) :: theta
    real(real64), intent(in) :: target

    integer(int64), parameter :: max_iters = 999999999
    real(real64), parameter :: pi = 3.1415926535897932384626433832795028_real64
    real(real64), parameter :: dt = 1.0e-6_real64
    real(real64), parameter :: v0 = 10.0_real64
    real(real64), parameter :: D = 0.1_real64

    integer(int64) :: search_i, simulate_i
    real(real64) :: minimum_angle, maximum_angle
    real(real64), dimension(2) :: v, r, F_d, F_g, a

    minimum_angle = 0.0_real64
    maximum_angle = pi / 4_real64
    theta = (minimum_angle + maximum_angle) / 2_real64


    search: do search_i = 1, max_iters

      v = [v0*cos(theta), v0*sin(theta)]
      r = [0.0_real64, 0.0_real64]

      simulate: do simulate_i = 1, max_iters
          r = r + v*dt

          ! Air resistance.
          F_d = - D * sum(v*v) * v / norm2(v)
          ! Gravity.
          F_g = [0.0_real64, -9.80665_real64]

          ! Assuming mass is 1.
          a = F_d + F_g

          v = v + a*dt

          if (r(2) <= 0) then
            exit simulate
          end if
      end do simulate

      if (abs(target - r(1)) <= 1.0e-3_real64) then
          return
      else if (r(1) < target) then
        minimum_angle = theta
      else if (r(1) > target) then
        maximum_angle = theta
      else
        error stop "A mistake has been made (other branches should account for all possibilities)."
      end if

      if (abs(minimum_angle - maximum_angle) <= 1.0e-6_real64) then
        exit search
      else
        theta = (minimum_angle + maximum_angle) / 2_real64
      end if
    end do search

    error stop "Failed to solve."
  end function solve
end program main
