program main
  use forlab, only: pi, rms, RPRE

  implicit none

  integer, parameter :: n = 100

  real(RPRE), dimension(n) :: x, y
  real(RPRE) :: result
  integer :: i

  x = [(2.0_RPRE * pi * real(i - 1, RPRE) / real(n - 1, RPRE), i=1,n)]
  y = sin(x)
  result = rms(y)

  if (abs(result - 0.7) >= 0.1) then
    error stop "Failed to use rms correctly!"
  end if
end program main
