program main
  use fftpack, only: rk, fft

  implicit none

  real(rk), parameter :: eps = 1.0e-10_rk
  complex(kind=rk), dimension(3) :: x

  x = [1.0_rk, 2.0_rk, 3.0_rk]
  if (sum(abs(fft(x, 2) - [(3.0_rk, 0.0_rk), (-1.0_rk, 0.0_rk)])) > eps) then
    error stop "FFT gave wrong result!"
  end if
end program main
