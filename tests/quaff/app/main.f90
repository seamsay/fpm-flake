program main
  use iso_varying_string, only: char
  use quaff, only: &
    fallible_length_t, &
    fallible_length_unit_t, &
    length_t, &
    length_unit_t, &
    parse_length, &
    parse_length_unit

  implicit none

  type(fallible_length_t) :: maybe_length
  type(length_t) :: length
  type(fallible_length_unit_t) :: maybe_length_unit
  class(length_unit_t), allocatable :: length_unit

  maybe_length = parse_length("1 m")
  if (maybe_length%failed()) then
    error stop "Length is statically valid!"
  end if

  length = maybe_length%length()

  maybe_length_unit = parse_length_unit("cm")
  if (maybe_length_unit%failed()) then
    error stop "Length unit is statically valid!"
  end if

  length_unit = maybe_length_unit%unit()

  if (char(length%to_string_in(length_unit)) /= "100.0 cm") then
    error stop "Failed to use quaff!"
  end if
end program main
