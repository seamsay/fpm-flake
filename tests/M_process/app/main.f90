program main
  use M_process, only: process_readall

  implicit none

  character(len=:), allocatable :: string
  integer :: ierr

  string = process_readall("echo hi", ierr = ierr)

  if (string /= "hi") then
    error stop "Failed to use M_process!"
  end if
end program main
