program main
  use M_calculator, only: inum0

  implicit none

  integer :: result

  result = inum0('20/2')

  if (result /= 10) then
    error stop "Failed to use inum0!"
  end if
end program main
