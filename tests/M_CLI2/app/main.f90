program main
  use M_CLI2, only: set_args, iget

  implicit none

  integer :: value

  call set_args("-x 10")
  value = iget("x")

  if (value /= 10) then
    error stop "Failed to use M_CLI2!"
  end if
end program main
