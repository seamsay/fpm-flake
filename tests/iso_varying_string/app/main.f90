program main
  use iso_varying_string, only: assignment(=), char, varying_string

  implicit none

  type(varying_string) :: s
  character(len=10) :: c, o

  o = "0123456789"
  s = o
  c = char(s, 10)

  if (c /= o) then
    error stop "Failed to use char!"
  end if
end program main
