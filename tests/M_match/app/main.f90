program main
  use M_match, only: getpat, match, regex_pattern, ERR, NO

  implicit none

  type(regex_pattern) :: regex
  character(len=*), parameter :: pattern = "ab*c", target = "abbc"

  if (getpat(pattern, regex%pat) == ERR) then
    error stop "Invalid pattern!"
  end if

  if (match(target, regex%pat) == NO) then
    error stop "Pattern doesn't match!"
  end if
end program main
