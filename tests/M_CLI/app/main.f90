program main
  use M_CLI, only: commandline, check_commandline

  implicit none

  character(len=:), allocatable :: command_line_string
  character(len=256) :: message
  integer :: ios
  integer :: x

  namelist /args/ x

  command_line_string = commandline('-x 10')
  read(command_line_string, nml=args, iostat=ios, iomsg=message)
  call check_commandline(ios, message)

  if (x /= 10) then
    error stop "Failed to use M_CLI properly!"
  end if
end program main
