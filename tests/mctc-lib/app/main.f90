program main
  use mctc_io, only: to_symbol

  implicit none

  if (to_symbol(1) /= "H") then
    error stop "Failed to use mctc-lib!"
  end if
end program main
