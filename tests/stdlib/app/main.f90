program main
  use stdlib_math, only: gcd

  implicit none

  if (gcd(48, 18) /= 6) then
    error stop "Failed to use stdlib!"
  end if
end program main
