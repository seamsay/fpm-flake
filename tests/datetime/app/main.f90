program main
  use datetime_module, only: datetime

  implicit none

  type(datetime) :: date

  date = date%now()

  print *, date%isoformat()
end program main
