program main
  use linear_interpolation_module, only: finterp_rk, linear_interp_1d

  implicit none

  integer, parameter :: n = 2

  real(finterp_rk), dimension(n) :: x
  real(finterp_rk), dimension(n) :: y
  type(linear_interp_1d) :: interpolator
  integer :: iflag, i
  real(finterp_rk) :: value

  x = [(real(i-1, finterp_rk)/real(n-1, finterp_rk), i=1,n)]
  y = x**2

  call interpolator%initialize(x, y, iflag)
  if (iflag /= 0) then
    error stop "Failed to initialise interpolator!"
  end if

  call interpolator%evaluate(0.5_finterp_rk, value)
  if (abs(value - 0.5_finterp_rk) > 1.0e-6_finterp_rk) then
    error stop "Incorrect value!"
  end if

  call interpolator%destroy()
end program main
