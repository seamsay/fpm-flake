program main
  use fhash, only: fhash_tbl_t, fhash_key

  implicit none

  type(fhash_tbl_t) :: table
  integer :: value

  call table%set(fhash_key("a"), 10)
  call table%get(fhash_key("a"), value)

  if (value /= 10) then
    error stop "Failed to use fhash correctly!"
  end if
end program main
