program main
  use pointsets, only: arithmetic_spacing

  implicit none

  real, dimension(10) :: a, b
  integer :: i

  call arithmetic_spacing(a, 1.0, 10.0)
  b = [(real(i), i=1,10)]

  if (any(a /= b)) then
    error stop "Failed to use pointsets!"
  end if
end program main
